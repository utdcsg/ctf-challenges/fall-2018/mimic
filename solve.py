import click
import sys
from pwnlib.tubes.process import process
from pwnlib.tubes.remote import remote

def solve(tube):
    tube.recvline() # Remove header
    value = tube.recvline()
    while '{' not in value:
        tube.send(value)
        value = tube.recvline()
    return value

def test_process(tube):
    expected_flag = 'csg{07734_1TS_m3}'
    found_flag = solve(tube)
    return expected_flag == found_flag.strip()

@click.command()
@click.option('--host', default='localhost', help='Remote host')
@click.option('--port', default=1337, help='Remote port')
def main(host, port):
    tube = remote(host, port)
    success = test_process(tube)
    if success:
        print "Success"
        sys.exit(0)
    else:
        print "Fail"
        sys.exit(1)

if __name__ == '__main__':
    main()
