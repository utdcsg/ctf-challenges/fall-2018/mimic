Mimic
=====

Category
--------
Misc

Suggested Point Value
---------------------
100

Author
------
Nick McClendon

Description
-----------
A simple socket server! Send what you get enough, and maybe you'll get a flag!

Solution
---------
Write a simple socket client that will send back what it recieves.

Flag
----

    csg{07734_1TS_m3}

Flag Modifiable
---------------
Yes, via Docker environment variable
