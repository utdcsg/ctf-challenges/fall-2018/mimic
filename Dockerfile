FROM golang:1.10.3 AS builder

RUN curl -fsSL -o /usr/local/bin/dep https://github.com/golang/dep/releases/download/v0.5.0/dep-linux-amd64 && chmod +x /usr/local/bin/dep

WORKDIR /go/src/mimic/

COPY Gopkg.toml Gopkg.lock ./

RUN dep ensure -vendor-only

COPY *.go ./

# This magic makes scratch containers work
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o mimic .

FROM scratch
ENV MIMIC_PORT 1337
ENV MIMIC_FLAG csg{07734_1TS_m3}

COPY --from=builder /go/src/mimic/mimic /mimic

ENTRYPOINT ["/mimic"]
