package main

import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"net"
	"strconv"
	"strings"

	"github.com/kelseyhightower/envconfig"
)

type challengeConfig struct {
	Port int
	Flag string
}

func handleConn(flag string, conn net.Conn) {
	conn.Write([]byte("Echo what I send to you to get the flag!\n"))
	connReader := bufio.NewReader(conn)
	for i := 0; i < 1000; i++ {
		sendValue := strconv.Itoa(rand.Intn(10000000000))
		conn.Write([]byte(sendValue + "\n"))
		recv, err := connReader.ReadString('\n')
		if err != nil {
			return
		}
		recvValue := strings.TrimRight(recv, "\r\n")
		if strings.Compare(sendValue, recvValue) != 0 {
			conn.Write([]byte("That's not what I said.\n"))
			return
		}
	}
	conn.Write([]byte(flag + "\n"))
}

func main() {
	var c challengeConfig
	err := envconfig.Process("mimic", &c)
	if err != nil {
		log.Fatal(err.Error())
	}
	log.Printf("Listening on port: %d", c.Port)
	ln, err := net.Listen("tcp", fmt.Sprintf(":%d", c.Port))
	if err != nil {
		log.Fatal(err.Error())
	}
	for {
		conn, _ := ln.Accept()
		go handleConn(c.Flag, conn)
	}

}
